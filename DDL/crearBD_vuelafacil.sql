create database vuelafacil; 
use vuelafacil; 
create table usuario(
documento_usu varchar(20) not null, 
nombre_usu varchar(50) not null, 
edad_usu varchar(3) not null, 
genero_usu varchar(1) not null,
nacionalidad_usu varchar(50) not null, 
constraint usuario_pk  primary key(documento_usu), 
constraint usuario_genero_usu_ck check(genero_usu='M' or genero_usu='F' or genero_usu='N')); 
create table vuelo(
numero_vuelo varchar(50) not null, 
fecha_vuelo date not null, 
hora_vuelo  time not null, 
constraint vuelo_pk primary key(numero_vuelo)); 
create table destinos( 
codigo_des varchar(20) not null, 
nombre_des varchar(50) not null, 
constraint destinos_pk primary key(codigo_des)); 
create table reserva_vuelo(
numero_res varchar(20) not null, 
numero_vuelo varchar(20) not null, 
documento_usu varchar(20) not null, 
destino_orig varchar(50) not null, 
destino_des varchar(50) not null, 
fecha_res date not null, 
precio_res double not null, 
constraint reserva_vuelo_pk primary key(numero_res), 
constraint reserva_vuelo_numero_vuelo_fk foreign key(numero_vuelo) references vuelo(numero_vuelo), 
constraint reserva_vuelo_documento_usu_fk foreign key(documento_usu) references usuario(documento_usu), 
constraint reserva_vuelo_destino_orig_fk foreign key(destino_orig) references destinos(codigo_des), 
constraint reserva_vuelo_destino_des_fk foreign key(destino_des) references destinos(codigo_des)); 
 















  

   


